from django.contrib import admin

# Register your models here.
from django.urls import reverse
from .models import Media


@admin.register(Media)
class MediaOpts(admin.ModelAdmin):
    list_display = ('__str__', 'get_facebook_url')

    def get_facebook_url(self, obj):
        url = reverse('catalog:media_detail_short', kwargs={'pk': obj.pk})
        link = '<a href="{}">View</a>'.format(url)
        return link
    get_facebook_url.allow_tags = True
