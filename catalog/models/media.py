from django.db import models


class Media(models.Model):
    """
    Модель медиаконтента
    """
    title = models.CharField(max_length=1000, blank=True, null=True)
    author = models.CharField(max_length=1000, blank=True, null=True)
    teaser = models.TextField(max_length=1000, blank=True, null=True)
    summary = models.TextField(blank=True, null=True)
    full_version = models.TextField(blank=True, null=True)
    cover_media = models.URLField(blank=True, null=True)