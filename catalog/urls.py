from django.conf.urls import url

from . import views

urlpatterns = [


    url(r'^m_list/$',
        views.MediaBotStateListView.as_view(),
        name='media_list'),

    url(r'^m_detail_short/(?P<pk>\d+)/$',
        views.MediaBotStateShortDetailView.as_view(),
        name='media_detail_short'),

    url(r'^m_detail_long/(?P<pk>\d+)/$',
        views.MediaBotStateLongDetailView.as_view(),
        name='media_detail_long'),

]
