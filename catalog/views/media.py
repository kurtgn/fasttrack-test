# Create your views here.
import json
from django.http import JsonResponse
from django.views.generic import ListView, DetailView
from django.template import Context, Template
import jsonpickle
from ..models import Media
from state_templates.models import BotStateTemplate
# from sending.state_objects import BotState, TGTextMessage


class BotStateTemplateResponseMixin(object):
    """
    Миксин, который достает шаблон, соответствующий вьюхе,
    и рендерит Json с этим шаблоном.
    """
    def get_context_data(self, **kwargs):
        """
        если запрос сделан с GET-параметром postback_prefix, добавить его
        в контект. И тогда в шаблонах он будет добавлен ко всем
        генерируемым урлам.

        Например, урл /cat/media_list/ превратится
        в /some/prefix/cat/media_list/)
        """
        context = super().get_context_data(**kwargs)
        context['postback_prefix'] = self.request.GET.get('postback_prefix', '')
        return context


    def render_to_response(self, context, **response_kwargs):
        """
        Получаем шаблон, имя которого соответствует имени текущего класса,
        рендерим с контекстом джанго, возвращаем в json
        """
        template_string = BotStateTemplate.objects.get(
            view_name=self.__class__.__name__
        ).facebook_template

        rendered = Template(template_string).render(Context(context))

        return JsonResponse(json.loads(rendered))


class MediaBotStateListView(BotStateTemplateResponseMixin, ListView):
    """
    представление списка объектов
    """
    model = Media


class MediaBotStateShortDetailView(BotStateTemplateResponseMixin, DetailView):
    """
    короткое представление одного объекта
    """
    model = Media


class MediaBotStateLongDetailView(BotStateTemplateResponseMixin, DetailView):
    """
    длинное представление одного объекта
    """
    model = Media

