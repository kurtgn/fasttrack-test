import logging

from django.conf import settings
import redis
import requests
from requests.exceptions import HTTPError


class LinkShortener(object):
    """Укорачиватель ссылок при помощи сервиса goo.gl.
    Все созданные ссылки кешируются в Redis.
    """
    api_url = 'https://www.googleapis.com/urlshortener/v1/url'

    def __init__(self):
        try:
            self.api_key = settings.GOOGLE_API_KEY
        except KeyError:
            raise Exception('Please specify GOOGLE_API_KEY '
                            'environment variable for link shortener.')
        try:
            redis_url = settings.REDIS_URL
        except KeyError:
            raise Exception('Please specify REDIS_URL environment variable '
                            'for link shortener.')
        self.redis = redis.from_url(redis_url)
        self.logger = logging.getLogger('main')

    def shorten(self, link):
        """Укоротить ссылку. Сначала попытаться достать ее из Redis,
        а если не удалось - запросить API (и сохранить результат в Redis)
        """
        from_redis = self.redis.get(link)
        if from_redis:
            self.logger.debug('link {} found in Redis. Returning'.format(link))
            return from_redis.decode('utf8')
        try:
            shortened = self._request_api(link)
        except HTTPError:
            # В случае неудачи (длина линка > 2047, кривой линк)
            # возвращаем линк нетронутым
            return link
        self.redis.set(link, shortened)
        self.logger.debug('Requested API, put to Redis: {} -> {}'
                          .format(link, shortened))
        return shortened

    def _request_api(self, link):
        """Сделать запрос к Google API для укорачивания ссылки
        """
        result = requests.post(
            self.api_url,
            headers={'Content-Type': 'application/json'},
            params={'key': self.api_key},
            json={'longUrl': link}
        )
        result.raise_for_status()
        shortened = result.json()['id']
        return shortened
