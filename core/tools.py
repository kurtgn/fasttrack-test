from urllib.parse import urlparse, parse_qsl, urlencode


def encode_query_params(url):
    """
    Энкодировать все GET-параметры строки, чтобы в ней был только один
    знак вопроса и было понятно, где начинаются и заканчиваются GET-параметры.

    Например, из http://a.b?url=http://backend.api?json=1
    сделать http://a.b?url=http%3A%2F%2Fbackend.api%3Fjson%3D1
    """
    parsed = urlparse(url)
    params = urlencode(parse_qsl(parsed.query))
    restored_url = ''
    restored_url += parsed.scheme + '://'
    restored_url += parsed.netloc + parsed.path
    if parsed.params:
        restored_url += ';' + parsed.params
    if params:
        restored_url += '?' + params
    if parsed.fragment:
        restored_url += '#' + parsed.fragment
    return restored_url