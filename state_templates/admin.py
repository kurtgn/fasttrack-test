from django.contrib import admin

# Register your models here.
from django.urls import reverse
from state_templates.models import BotStateTemplate


@admin.register(BotStateTemplate)
class BotStateTemplateOpts(admin.ModelAdmin):
    list_display = ('__str__', 'get_edit_link')

    def get_edit_link(self, obj):
        url = reverse('state_templates:template_update', kwargs={'pk': obj.pk})
        return '<a href="{}">Open</a>'.format(url)
    get_edit_link.allow_tags = True