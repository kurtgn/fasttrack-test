from django.db import models


class BotStateTemplate(models.Model):
    """
    Шаблоны для вьюх. Имеют названия (соответствует классам вьюх),
    и содержат контент для всех платформ.
    """
    view_name = models.CharField(max_length=100)
    telegram_template = models.TextField()
    facebook_template = models.TextField()
    viber_template = models.TextField()

    def __str__(self):
        return self.view_name