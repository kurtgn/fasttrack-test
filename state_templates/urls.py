from django.conf.urls import url

from . import views

urlpatterns = [


    url(r'^template_update/(?P<pk>\d+)/$',
        views.StateTemplateView.as_view(),
        name='template_update'),

]
