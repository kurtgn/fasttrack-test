from django.http import HttpResponse
from django.shortcuts import render

# Create your views here.
from django.urls import reverse
from django.views.generic import DetailView, UpdateView
from state_templates.models import BotStateTemplate


class StateTemplateView(UpdateView):
    model = BotStateTemplate
    fields = ('view_name', 'facebook_template')
    template_name = 'state_templates/state_template_update.html'

    def form_valid(self, form):
        return HttpResponse('Редактировать этот шаблон запрещено.')

    def get_success_url(self):
        return reverse('state_templates:template_update', kwargs={
            'pk': self.object.pk
        })