from django.apps import AppConfig


class StateTemplatesConfig(AppConfig):
    name = 'state_templates'
