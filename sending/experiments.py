from sending.state_objects import FBCarouselMessage, BotState, FBCard, \
    FBButtonGroup, FBButton, FBTextMessage, FBQRButtonGroup, FBQRButton

state = BotState(
    messages=[
        FBCarouselMessage(
            cards=[
                FBCard(
                    title='sometitle',
                    subtitle='subtitle',
                    button_group=FBButtonGroup(
                        buttons=[
                            FBButton(
                                title='btn title',
                                payload='123',
                                type='postback'
                            ),
                            FBButton(
                                title='btn title2',
                                payload='1232',
                                type='postback'
                            ),
                        ]

                    )
                )
            ]
        )
    ]
)


import jsonpickle
import json



state2 = BotState(
    messages=[FBTextMessage(text='Choose item:')],
    qr_buttons=FBQRButtonGroup(
        buttons=[
            FBQRButton(
                title='title',
                payload='payload'
            )
        ]
    )
)



print(json.dumps(json.loads(jsonpickle.encode(state2)), indent=4))