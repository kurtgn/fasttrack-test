class Message(object):
    """
    Класс, от которго наследуются все типы сообщений.
    """

    def not_empty(self):
        """
        Метод переопределяется в FBTextMessage и TGTextMessage.
        Во всех остальных случаях сообщение считается не пустым.
        """
        return True

    def class_name(self):
        return self.__class__.__name__
