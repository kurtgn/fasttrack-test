from django.template import Template
from core.tools import encode_query_params

from .tools import choose_random
from ..exceptions import StateObjectError
from .common import Message


class FBTextMessage(Message):
    """
    Текстовое сообщение. Содержит всего один атрибут: текст content.
    """
    def __init__(self, text):
        try:
            text = str(text)
        except:
            raise StateObjectError('Unable to convert text into str')
        if len(text) > 320:
            raise StateObjectError(
                'Content for TextMessage must be < 320 symbols')
        self.text = text

    def __repr__(self):
        return self.text[:30]

    def api_dict(self):
        data = {'text': self.text}
        return data

    def not_empty(self):
        if self.text:
            return True

    def render(self, context):
        """Отрендерить контент, вставив туда переменные из контекста.

        :param context: объект класса django.template.Context
        """
        self.text = choose_random(self.text)
        template = Template(self.text)
        self.text = str(template.render(context))


# XXX: НЕ РАБОТАЕТ
class FBImageMessage(Message):
    """
    Сообщение с изображением. Содержит атрибут: url
    """
    def __init__(self, url, caption=None):
        # caption нужен здесь как болванка, потому что иногда этот класс
        # вызывается с caption конструктором сообщений (для унификации
        # с Telegram)
        self.url = url

    def __repr__(self):
        return 'Image: {}...'.format(self.url[:10])

    def api_dict(self):
        data = {
            'attachment': {
                'type': 'image',
                'payload': {
                    'url': self.url
                }
            }
        }
        return data

    def render(self, context):
        """Рендерить тут нечего"""


class FBVideoMessage(Message):
    """
    Сообщение с видео. Содержит атрибут: url
    """
    def __init__(self, url, caption=None):
        # caption нужен здесь как болванка, потому что иногда этот класс
        # вызывается с caption конструктором сообщений (для унификации
        # с Telegram)
        self.url = url

    def __repr__(self):
        return 'Video: {}...'.format(self.url[:10])

    def api_dict(self):
        data = {
            'attachment': {
                'type': 'video',
                'payload': {
                    'url': self.url
                }
            }
        }
        return data

    def render(self, context):
        """Рендерить тут нечего"""


class FBDocumentMessage(Message):
    """
    Сообщение с документом. Содержит атрибут: url
    """
    def __init__(self, url, caption=None):
        # caption нужен здесь как болванка, потому что иногда этот класс
        # вызывается с caption конструктором сообщений (для унификации
        # с Telegram)
        self.url = url

    def __repr__(self):
        return 'Document: {}...'.format(self.url[:10])

    def api_dict(self):
        data = {
            'attachment': {
                'type': 'file',
                'payload': {
                    'url': self.url
                }
            }
        }
        return data

    def render(self, context):
        """Рендерить тут нечего"""


class FBAudioMessage(Message):
    """
    Сообщение с аудио. Содержит атрибут: url
    """
    def __init__(self, url, caption=None):
        # caption нужен здесь как болванка, потому что иногда этот класс
        # вызывается с caption конструктором сообщений (для унификации
        # с Telegram)
        self.url = url

    def __repr__(self):
        return 'Audio: {}...'.format(self.url[:10])

    def api_dict(self):
        data = {
            'attachment': {
                'type': 'audio',
                'payload': {
                    'url': self.url
                }
            }
        }
        return data

    def render(self, context):
        """Рендерить тут нечего"""


class FBButtonGroup(object):
    """
    Группа кнопок ФБ. Может содержать до 3 кнопок.
    """
    def __init__(self, buttons):
        if not buttons or len(buttons) == 0:
            raise StateObjectError('Cannot have 0 buttons')
        if len(buttons) > 3:
            raise StateObjectError(
                'Number of buttons in FBButtonGroup cannot be > 3')
        self.buttons = buttons

    def __repr__(self):
        return ', '.join(str(button) for button in self.buttons)

    def api_dict(self):
        return [button.api_dict() for button in self.buttons]


class FBButton(object):
    """
    Кнопка ФБ.
    """
    def __init__(self, title, type, payload, webview_height_ratio=None):
        types = {'postback', 'web_url', 'element_share', 'phone_number',
                 'custom_node', 'custom_text'}
        webview_height_ratios = {None, 'compact', 'tall', 'full'}
        if type not in types:
            raise StateObjectError('Wrong FBButton type: {}'.format(type))
        if webview_height_ratio not in webview_height_ratios:
            raise StateObjectError('Wrong webview_height_ratio: {}'
                                   .format(webview_height_ratios))
        if type != 'element_share' and len(title) > 20:
            raise StateObjectError('FBButton title must be < 20')
        if type != 'web_url' and webview_height_ratio:
            raise StateObjectError('webview_height_ratio only acceptable '
                                   'for web_urlbuttons')
        self.title = str(title)
        self.type = type
        self.payload = payload
        self.webview_height_ratio = webview_height_ratio

    def __repr__(self):
        return self.title

    def api_dict(self):
        if self.type == 'web_url':
            data = {'type': 'web_url',
                    'url': self.payload,
                    'title': self.title,
                    'webview_height_ratio': self.webview_height_ratio or 'full'}
        elif self.type == 'element_share':
            data = {'type': 'element_share'}
        elif self.type in ('custom_node', 'custom_text', 'postback'):
            data = {'type': 'postback', 'title': self.title,
                    'payload': self.payload}
        elif self.type == 'phone_number':
            data = {'type': 'phone_number', 'title': self.title,
                    'payload': self.payload}
        else:
            raise StateObjectError('Not supposed to get here')
        return data


class FBQRButton(object):
    """
    QR-кнопка ФБ.
    """
    def __init__(self, title, payload):
        if len(title) > 20:
            raise StateObjectError('FBButton title must be < 20')
        self.title = str(title)
        self.payload = payload

    def __repr__(self):
        return self.title

    def api_dict(self):
        data = {'content_type': 'text', 'title': self.title,
                'payload': self.payload}
        return data


class FBQRButtonGroup(object):
    """
    Группа кнопок Facebook. Может содержать до 10 кнопок
    """
    def __init__(self, buttons):
        if not isinstance(buttons, list):
            raise StateObjectError('Wrong format for button group')
        if len(buttons) == 0:
            raise StateObjectError('Cannot have 0 buttons')
        if not isinstance(buttons[0], FBQRButton):
            raise StateObjectError('Wrong format for button group')
        if len(buttons) > 10:
            raise StateObjectError('Button number must be <= 10')
        self.buttons = buttons

    def __repr__(self):
        return ', '.join(str(button) for button in self.buttons)

    def api_dict(self):
        data = [button.api_dict() for button in self.buttons]
        return data


class FBCard(object):
    """
    Карточка ФБ. Содержит заголовок, тело, ссылку на картинку,
    ссылку на страницу и группу кнопок FBButtonGroup.
    """
    def __init__(self, title, subtitle='', button_group=None,
                 image_url='', item_url=None):
        if len(title) > 80:
            raise StateObjectError('Title must be <= 80')
        if subtitle and len(subtitle) > 80:
            raise StateObjectError('Subtitle must be <= 80')
        if button_group and not isinstance(button_group, FBButtonGroup):
            raise StateObjectError('Wrong type of button group')
        self.title = str(title)
        self.subtitle = str(subtitle)
        self.button_group = button_group
        self.image_url = image_url
        self.item_url = item_url

    def __repr__(self):
        return self.title[:30]

    def api_dict(self):
        if self.button_group:
            buttons = self.button_group.api_dict()
        else:
            buttons = None
        data = {
            'title': self.title,
            'item_url': self.item_url,
            'image_url': self.image_url,
            'subtitle': self.subtitle,
            'buttons': buttons
        }
        data = {
            key: value for key, value in data.items() if value
        }
        return data


class FBCarouselMessage(Message):
    """
    Сообщение-карусель. Содержит список карточек типа FBCard.
    """
    def __init__(self, cards):
        if len(cards) > 10:
            raise StateObjectError('Number of cards cannot be > 10')
        self.cards = cards

    def __repr__(self):
        return '{} cards'.format(len(self.cards))

    def api_dict(self):
        data = {
            'attachment': {
                'type': 'template',
                'payload': {
                    'template_type': 'generic',
                    'elements': [
                        card.api_dict() for card in self.cards
                        ]
                }
            }
        }
        return data

    def render(self, context):
        """Отрендерить контент, вставив туда переменные из контекста.

        :param context: объект класса django.template.Context
        """
        for card in self.cards:
            title_template = Template(choose_random(card.title))
            subtitle_template = Template(choose_random(card.subtitle))
            card.title = str(title_template.render(context))
            if card.subtitle:
                card.subtitle = str(subtitle_template.render(context))
            if not card.button_group:
                return

            for button in card.button_group.buttons:
                if button.type == 'web_url':
                    # Все URL-кнопки рендерим с контекстом
                    # и энкодируем их GET-параметры
                    template = Template(button.payload)
                    button.payload = str(template.render(context))
                    button.payload = encode_query_params(button.payload)

    def not_empty(self):
        if self.cards:
            return True


class FBThreeButtonMessage(Message):
    """
    Сообщение с 3 кнопками Facebook. Содержит текст и кнопки.
    """
    def __init__(self, text, button_group):
        if len(text) > 320:
            raise StateObjectError('FBThreeButtonMessage must be < 320 symbols')
        if not isinstance(button_group, FBButtonGroup):
            raise StateObjectError('Wrong type of button group')
        self.text = str(text)
        self.button_group = button_group

    def __repr__(self):
        return self.text[:30]

    def api_dict(self):
        data = {
            'attachment': {
                'type': 'template',
                'payload': {
                    'template_type': 'button',
                    'text': self.text,
                    'buttons': self.button_group.api_dict()
                }
            }
        }
        return data

    def render(self, context):
        """Отрендерить контент, вставив туда переменные из контекста.

        :param context: объект класса django.template.Context
        """
        template = Template(choose_random(self.text))
        self.text = str(template.render(context))


class FBSleepMessage(Message):
    """
    Сообщение-пауза. Содержит значение паузы в секундах. Максимум - 20 секунд.
    """

    def __init__(self, seconds, show_typing=False):
        """
        :param seconds: сколько секунд держать паузу
        :param show_typing: показывать ли символ "печатаю..."
        """
        if not isinstance(seconds, int):
            raise StateObjectError(
                'WaitMessage arg must be integer in seconds')
        if seconds > 20:
            seconds = 20
        self.seconds = seconds
        self.show_typing = show_typing

    def __repr__(self):
        string = 'Typing' if self.show_typing else 'Sleep'
        string += ' {}'.format(self.seconds)
        return string

    def render(self, context):
        """Нечего рендерить"""
        pass

    def api_dict(self):
        return {'wait_seconds': self.seconds}
