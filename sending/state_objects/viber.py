from django.template import Template

from ..exceptions import StateObjectError
from .tools import choose_random
from .common import Message


class VBColors(object):
    DARK_PURPLE = '#59267c'
    LIGHT_PURPLE = '#8f5db7'
    PINK = '#e2d4e7'
    LIGHT_BLUE = '#a5cfd5'
    GREEN = '#81cd50'
    CUSTOM_GREY = '#777777'
    CUSTOM_WHITE = '#ffffff'
    CUSTOM_LIGHT_PINK = '#fcf2ff'
    CUSTOM_GREY_MENU_BG = '#e9e9e9'


def colored(string, color):
    """Обернуть текст в <font> определенного цвета"""
    return '<font color={}>{}</font>'.format(color, string)


def bold(string):
    return '<b>{}</b>'.format(string)


class VBTextMessage(Message):
    """
    Текстовое сообщение. Содержит всего один атрибут: текст content.
    """
    def __init__(self, text):
        self.text = str(text)
        self.text = self.text[:1024]

    def __repr__(self):
        return self.text[:30]

    def api_dict(self):
        data = {'text': self.text}
        return data

    def not_empty(self):
        if self.text:
            return True

    def render(self, context):
        """Отрендерить контент, вставив туда переменные из контекста.

        :param context: объект класса django.template.Context
        """
        template = Template(choose_random(self.text))
        self.text = str(template.render(context))


class VBImageMessage(Message):
    """
    Сообщение с изображением. Содержит атрибут: url, caption
    """
    def __init__(self, url, caption=None):
        if not caption:
            caption = ''
        if len(caption) > 120:
            raise StateObjectError('VBImageMessage.caption must be <= 120')
        self.url = url
        self.caption = str(caption)

    def __repr__(self):
        return 'Image: {}...'.format(self.url[:10])

    def api_dict(self):
        data = {
            'url': self.url,
            'caption': self.caption
        }
        return data

    def render(self, context):
        """ Рендерить тут нечего """
        pass


class VBDocumentMessage(Message):
    """
    Сообщение с файлом. Содержит атрибут: url
    """
    def __init__(self, url, caption=None):
        # caption нужен здесь как болванка, потому что иногда этот класс
        # вызывается с caption конструктором сообщений (для унификации
        # с Telegram и Facebook)
        self.url = url

    def __repr__(self):
        return 'Document: {}...'.format(self.url[:10])

    def get_file_name(self):
        """
        Получить имя файла из URL
        """
        return self.url.split('/')[-1]

    def api_dict(self):
        data = {
            'url': self.url,
        }
        return data

    def render(self, context):
        """ Рендерить тут нечего """
        pass


class VBQRButtonGroup(object):
    """
    Группа кнопок Viber. Должны быть списком
    """
    def __init__(self, buttons=None, restore_normal_keyboard=False,
                 bg_color=VBColors.CUSTOM_GREY_MENU_BG):
        """Здесь можно задать как QR-кнопки, так и указание
        спрятать старые кнопки и показать нормальную клавиатуру.
        """

        if restore_normal_keyboard and buttons:
            raise StateObjectError(
                'Please specify either restore_normal_keyboard or buttons, '
                'not both at the same time.')

        elif not restore_normal_keyboard:
            if not isinstance(buttons, list):
                raise StateObjectError('Wrong format for button group')
            if len(buttons) == 0:
                raise StateObjectError('Cannot have 0 buttons in VBQRButtonGroup')
            if not isinstance(buttons[0], VBQRButton):
                raise StateObjectError('Wrong format for button group')
            self.buttons = buttons
            self.restore_normal_keyboard = restore_normal_keyboard
            self.bg_color = bg_color
        else:
            self.buttons = buttons
            self.restore_normal_keyboard = restore_normal_keyboard

    def __repr__(self):
        return ', '.join(str(button) for button in self.buttons)

    def api_dict(self):
        if self.buttons:
            btn_data = [button.api_dict() for button in self.buttons]
            data = {
                'Type': 'keyboard',
                'DefaultHeight': False,
                'Buttons': btn_data,
                'BgColor': self.bg_color
            }
        else:
            data = None
        return data


class VBQRButton(object):
    """
    Кнопка Viber.
    """
    def __init__(self, columns, rows=1, data=None, text='', opacity=None,
                 bg_color=VBColors.CUSTOM_WHITE, horizontal_align=None,
                 vertical_align=None, type='reply', image=None, silent=False,
                 text_size=None, font_color='#000000'):
        if type not in {'reply', 'open-url', 'none'}:
            raise StateObjectError('Wrong VBQRButton type: {}'.format(type))
        if text_size not in {None, 'small', 'regular', 'large'}:
            raise StateObjectError('Wrong VBQRButton text size: {}'
                                   .format(text_size))
        if vertical_align not in {None, 'top', 'middle', 'bottom'}:
            raise StateObjectError('Wrong VBQRButton vertical_align: {}'
                                   .format(vertical_align))
        if horizontal_align not in {None, 'left', 'center', 'right'}:
            raise StateObjectError('Wrong VBQRButton horizontal_align: {}'
                                   .format(horizontal_align))

        if type is not 'none' and not data:
            raise StateObjectError('if VBQRButton.type is not "none", '
                                   'you need to supply VBQRButton.data')

        self.text = str(text)
        self.data = data
        self.type = type
        self.columns = columns
        self.rows = rows
        self.bg_color = bg_color
        self.font_color = font_color
        self.opacity = opacity
        self.horizontal_align = horizontal_align
        self.vertical_align = vertical_align
        self.image = image
        self.silent = silent
        self.text_size = text_size

    def __repr__(self):
        return self.data

    def api_dict(self):
        data = {
            'Columns': self.columns,
            'Rows': self.rows,
            'ActionType': self.type,
            'ActionBody': self.data,
        }
        if self.text:
            data['Text'] = colored(self.text, self.font_color)
        if self.image:
            data['Image'] = self.image
        if self.bg_color:
            data['BgColor'] = self.bg_color
        if self.opacity:
            data['TextOpacity'] = self.opacity
        if self.horizontal_align:
            data['TextHAlign'] = self.horizontal_align
        if self.vertical_align:
            data['TextVAlign'] = self.vertical_align
        if self.silent:
            data['Silent'] = self.silent
        if self.text_size:
            data['TextSize'] = self.text_size
        return data


class VBSleepMessage(Message):
    """
    Сообщение-пауза. Содержит значение паузы в секундах. Максимум - 20 секунд.
    """

    def __init__(self, seconds, show_typing=False):
        """
        :param seconds: сколько секунд держать паузу
        :param show_typing: болванка для унификации с Telegram и Facebook.
        """
        if not isinstance(seconds, int):
            raise StateObjectError('WaitMessage arg must be integer in seconds')
        if seconds > 20:
            seconds = 20
        self.seconds = seconds

    def __repr__(self):
        return 'Sleep {}'.format(self.seconds)

    @staticmethod
    def render(context):
        """Нечего рендерить"""
        pass

    def api_dict(self):
        return {'wait_seconds': self.seconds}


class VBCarouselMessage(Message):
    """Карусель вайбера. Содержит до 6 карточек."""

    def __init__(self, card_group, bg_color='#ffffff'):

        if not isinstance(card_group, VBCardGroup):
            raise StateObjectError(
                'Viber cards width must be of type VBCardGroup'
            )

        self.card_group = card_group

    def api_dict(self):
        data = {
            "ButtonsGroupColumns": self.card_group.card_width,
            "ButtonsGroupRows": self.card_group.card_height,
            "BgColor": "#FFFFFF",
            "Buttons": self.card_group.api_dict()
        }
        return data

    @staticmethod
    def render(context):
        """Пока ничего не рендерим. 18.03.2017"""
        pass


class VBCardGroup(object):
    """
    Группа карточек вайбера. Превращает список карточек в список Buttons
    согласно АПИ вайбера
    """
    def __init__(self, cards, card_width=6, card_height=7):
        if card_width <= 0 or card_width > 6:
            raise StateObjectError('Viber card width must be 1...6')
        if card_width <= 0 or card_height > 7:
            raise StateObjectError('Viber card width must be 1...7')
        if not isinstance(cards, list):
            raise StateObjectError('Viber card list must be of type list')
        if len(cards) > 6:
            raise StateObjectError('Viber card amount must be <= 6')
        self.cards = cards
        self.card_width = card_width
        self.card_height = card_height

        # Добавляем каждой карточке указатель на родительскую группу,
        # чтобы у них был доступ к card_width и card_height
        for card in self.cards:
            card.group = self

            # Проверяем, что в каждой карточке
            # кнопки соответствуют ширине карточки
            for btn in card.buttons:
                if btn.columns != card_width:
                    raise StateObjectError(
                        'Card group of width {} must have buttons '
                        'of same width, not {}'.format(
                            self.card_width, btn.columns)
                    )

    def api_dict(self):
        """
        Собрать результаты вызова to_buttons() во всех карточках
        в единый список, перевести в dict
        """
        buttons = []
        for card in self.cards:
            buttons += card.to_buttons()
        api_dict = [btn.api_dict() for btn in buttons]
        return api_dict


class VBCard(object):
    """
    Сообщение-карточка вайбера
    """

    def __init__(self, title, description, buttons,
                 image_height_rows=3,
                 image=None,
                 item_url=None,
                 title_color=VBColors.DARK_PURPLE,
                 description_color=VBColors.CUSTOM_GREY):
        """
        :param image: картинка для карточки (рендерится высотой в 3 строки)
        :param title: заголовок (рендерится жирным)
        :param description: описание (рендерится нежирным, с новой строки)
        :param item_url: если передан - то по клику на карточку
            открывается URL
        """
        if not isinstance(buttons, list):
            raise StateObjectError('VBCard.buttons must be of type list')
        if len(buttons) > 3:
            raise StateObjectError('VBCard.buttons amount must be <= 3')
        self.image = image
        if image:
            self.image_height_rows = image_height_rows
        else:
            self.image_height_rows = 0
        self.title = str(title)
        self.title_color = title_color
        self.description = str(description)
        self.description_color = description_color
        self.buttons = buttons
        self.item_url = item_url
        self._group = None

    @property
    def group(self):
        """
        Делаем ссылку на группу через @property, чтобы иметь возможность
        сообщить пользователю, если он пытается невовремя получить доступ
        к группе.
        """
        if not self._group:
            raise AttributeError(
                'This VBCard is not bound to a group and thus does not have '
                'access to .group attribute.'
            )
        return self._group

    @group.setter
    def group(self, value):
        self._group = value

    def to_buttons(self):
        """
        Превратить объект карточки в вайберовский список Buttons
        """

        text_height_rows = (
            self.group.card_height - len(self.buttons) - self.image_height_rows
        )
        text = (
            bold(colored(self.title, self.title_color))
            + '<br/>'
            + colored(self.description, self.description_color)
        )
        text_btn = VBQRButton(
            text=text,
            vertical_align='top',
            horizontal_align='left',
            columns=self.group.card_width,
            rows=text_height_rows,
            type='none',
            text_size='small',
            silent=True
        )

        all_buttons = [text_btn] + self.buttons

        if self.image:
            image_btn = VBQRButton(
                image=self.image,
                rows=self.image_height_rows,
                columns=self.group.card_width,
                type='none',
                silent=True
            )
            if self.item_url:
                image_btn.type = text_btn.type = 'open-url'
                image_btn.data = text_btn.data = self.item_url

            all_buttons = [image_btn] + all_buttons

        return all_buttons





