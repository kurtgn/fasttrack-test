from django.template import Template
from telepot.namedtuple import ReplyKeyboardMarkup, ReplyKeyboardHide

from core.link_shortener import LinkShortener
from core.tools import encode_query_params
from .tools import choose_random
from ..exceptions import StateObjectError
from .common import Message


class TGTextContent(object):
    """
    Текстовое сообщение Telegram. Содержит всего один атрибут: текст content.
    """
    def __init__(self, text):
        self.text = str(text)

    def __repr__(self):
        return self.text[:30]

    def api_dict(self):
        data = {'text': self.text}
        return data

    def not_empty(self):
        if self.text:
            return True

    def render(self, context):
        """Отрендерить контент, вставив туда переменные из контекста.

        :param context: объект класса django.template.Context
        """
        self.text = choose_random(self.text)
        template = Template(self.text)
        self.text = str(template.render(context))


class TGImageContent(object):
    """
    Контент с изображением Telegram. Содержит два атрибута: url и caption.
    """
    def __init__(self, url, caption=None):
        if not caption:
            caption = ''
        self.caption = str(caption)
        if len(self.caption) > 200:
            raise StateObjectError(
                'TGImageContent caption must be <= 200 symbols')
        self.url = url

    def __repr__(self):
        return 'Image: {}... - {}'.format(self.url[:10], self.caption)

    def api_dict(self):
        data = {
            'url': self.url,
            'caption': self.caption
        }
        return data

    def render(self, context):
        """Отрендерить контент, вставив туда переменные из контекста.

        :param context: объект класса django.template.Context
        """
        self.caption = choose_random(self.caption)
        template = Template(self.caption)
        self.caption = str(template.render(context))


class TGDocumentContent(object):
    """
    Контент с документом Telegram. Содержит два атрибута: url и caption.
    """
    def __init__(self, url, caption=None):
        if not caption:
            caption = ''
        if len(caption) > 200:
            raise StateObjectError(
                'Document caption must be <= 200 symbols'
            )
        self.url = str(url)
        self.caption = str(caption)

    def __repr__(self):
        return 'File: {}... - {}'.format(self.url[:10], self.caption)

    def api_dict(self):
        data = {
            'document': self.url,
            'caption': self.caption
        }
        return data

    def render(self, context):
        """Отрендерить контент, вставив туда переменные из контекста.

        :param context: объект класса django.template.Context
        """
        self.caption = choose_random(self.caption)
        template = Template(self.caption)
        self.caption = str(template.render(context))


class TGInlineButtonGroup(object):
    """
    Инлайн группа кнопок Telegram. Должна быть списком списков.
    """
    def __init__(self, buttons):
        if not isinstance(buttons, list):
            raise StateObjectError('Wrong format for button group')
        if not isinstance(buttons[0], list):
            raise StateObjectError('Wrong format for button group')
        if not isinstance(buttons[0][0], TGInlineButton):
            raise StateObjectError('Wrong format for button group')
        self.buttons = buttons

    def __repr__(self):
        return ', '.join(str(button) for row in self.buttons for button in row)

    def api_dict(self):
        data = [
            [button.api_dict() for button in row]
            for row in self.buttons
        ]
        return data


class TGInlineButton(object):
    """
    Инлайн кнопка Telegram
    """
    def __init__(self, text, type, payload=None, inline_query=''):
        allowed_types = {
            'postback', 'url', 'custom_node', 'custom_text', 'share'
        }
        if type not in allowed_types:
            raise StateObjectError(
                'Wrong TGInlineButton type: {}'.format(type)
            )

        # Проверка на длину пейлоада (неактуальна пока что, т.к. у многих
        # текущих кнопок пейлоад и так длинный)
        # if (type in ('postback', 'custom_node', 'custom_text')
        #         and len(payload.encode('utf8')) > 64):
        #     raise StateObjectError(
        #         'Too long TGInlineButton payload: {}'.format(payload)
        #     )
        self.text = str(text)
        self.type = type
        self.payload = payload
        self.inline_query = inline_query

    def __repr__(self):
        return self.text

    def api_dict(self):
        data = {'text': self.text}
        if self.type in ('postback', 'custom_node', 'custom_text'):
            data.update({'callback_data': self.payload})
        if self.type == 'share':
            data.update({'switch_inline_query': self.inline_query})
        if self.type == 'url':
            data.update({'url': self.payload})
        return data


class TGMessageMethodsMixin(object):
    """
    Миксин для работы с сообщениями Telegram
    """

    def __repr__(self):
        return str(self.content)

    def render(self, context):
        """Отрендерить контент, вставив туда переменные из контекста.

        :param context: объект класса django.template.Context
        """
        self.content.render(context)
        if not self.inline_buttons:
            return

        for row in self.inline_buttons.buttons:
            for button in row:
                if button.type == 'url':
                    # Все URL-кнопки рендерим с контекстом
                    # и энкодируем их GET-параметры
                    template = Template(button.payload)
                    button.payload = str(template.render(context))
                    button.payload = encode_query_params(button.payload)
                    button.payload = LinkShortener().shorten(button.payload)

    def api_dict(self):
        if self.inline_buttons:
            buttons = self.inline_buttons.api_dict()
        else:
            buttons = None
        data = {
            'message': self.content.api_dict(),
            'inline_buttons': buttons
        }
        return data

    def content_type(self):
        return self.content.content_type()


class TGTextMessage(TGMessageMethodsMixin, Message):
    """СОобщение с текстовым контентом
    Должно содержать:
        - контент (текст)
        - (опционально) ID сообщения, которое нужно заменить
                        этим новым сообщением
        - (опционально) инлайн-кнопки (список списков)."""

    def __init__(self, text, inline_buttons=None, replace_telegram_msg=None,
                 restore_qwerty_keyboard=False):
        if len(text) > 4096:
            raise StateObjectError('TGTextMessage text must be < 4096 symbols')
        if inline_buttons and restore_qwerty_keyboard:
            raise StateObjectError('Cannot have inline buttons '
                                   '+ restore qwerty keyboard')
        self.text = str(text)
        self.content = TGTextContent(text=self.text)
        self.inline_buttons = inline_buttons
        self.replace_telegram_msg = replace_telegram_msg
        self.restore_qwerty_keyboard = restore_qwerty_keyboard

    def not_empty(self):
        return self.content.not_empty()


class TGImageMessage(TGMessageMethodsMixin, Message):
    """Сообщение с изображением
    Должно содержать:
        - контент (картинку)
        - (опционально) ID сообщения, которое нужно заменить
                        этим новым сообщением
        - (опционально) инлайн-кнопки (список списков).
    """
    def __init__(self, url, caption=None, inline_buttons=None,
                 replace_telegram_msg=None, restore_qwerty_keyboard=False):
        if inline_buttons and restore_qwerty_keyboard:
            raise StateObjectError('Cannot have inline buttons '
                                   '+ restore qwerty keyboard')
        self.content = TGImageContent(url=url, caption=caption)
        self.inline_buttons = inline_buttons
        self.replace_telegram_msg = replace_telegram_msg
        self.restore_qwerty_keyboard = restore_qwerty_keyboard


class TGDocumentMessage(TGMessageMethodsMixin, Message):
    """Сообщение с документом
    Должно содержать:
        - контент (документ)
        - (опционально) ID сообщения, которое нужно заменить
                        этим новым сообщением
        - (опционально) инлайн-кнопки (список списков).
    """
    def __init__(self, url, caption=None, inline_buttons=None,
                 replace_telegram_msg=None, restore_qwerty_keyboard=False):
        if inline_buttons and restore_qwerty_keyboard:
            raise StateObjectError(
                'Cannot have inline buttons + restore qwerty keyboard'
            )
        self.content = TGDocumentContent(url=url, caption=caption)
        self.inline_buttons = inline_buttons
        self.replace_telegram_msg = replace_telegram_msg
        self.restore_qwerty_keyboard = restore_qwerty_keyboard


class TGQRButtonGroup(object):
    """
    Группа кнопок Telegram. Должны быть списком списков
    """
    def __init__(self, buttons=None, restore_normal_keyboard=False):
        """Здесь можно задать как QR-кнопки, так и указание
        спрятать старые кнопки и показать нормальную клавиатуру.
        """

        if restore_normal_keyboard and buttons:
            raise StateObjectError(
                'Please specify either restore_normal_keyboard or buttons, '
                'not both at the same time.')

        elif not restore_normal_keyboard:
            if not isinstance(buttons, list):
                raise StateObjectError('Wrong format for button group')
            if len(buttons) == 0:
                raise StateObjectError(
                    'Cannot have 0 buttons in TGQRButtonGroup')
            if not isinstance(buttons[0], list):
                raise StateObjectError('Wrong format for button group')
            if not isinstance(buttons[0][0], TGQRButton):
                raise StateObjectError('Wrong format for button group')
            self.buttons = buttons
            self.restore_normal_keyboard = restore_normal_keyboard
        else:
            self.buttons = buttons
            self.restore_normal_keyboard = restore_normal_keyboard

    def __repr__(self):
        return ', '.join(str(button) for row in self.buttons for button in row)

    def api_dict(self):
        if self.buttons:
            data = [
                [button.api_dict() for button in row]
                for row in self.buttons
            ]
        else:
            data = {'buttons': 'hide QR keyboard'}
        return data

    def generate_markup(self):
        """Вернуть либо markup c новыми QR-кнопками, либо инструкцию спрятать
        старые QR-кнопки, либо просто ничего не делать (и сендер тогда
        сохранит старый markup)
        """
        if self.buttons:
            qr_buttons = self.api_dict()
            markup = ReplyKeyboardMarkup(keyboard=qr_buttons,
                                         resize_keyboard=True,
                                         one_time_keyboard=True)
        elif self.restore_normal_keyboard:
            markup = ReplyKeyboardHide(hide_keyboard=True)
        else:
            raise StateObjectError('not supposed to get here')
        return markup


class TGQRButton(object):
    """
    Группа кнопок Telegram. Должны быть списком списков
    """
    def __init__(self, text, type='text'):
        types = {'text', 'request_contact', 'request_location'}
        if type not in types:
            raise StateObjectError('Wrong TQButton type: {}'.format(type))
        self.text = str(text)
        self.type = type

    def __repr__(self):
        return self.text

    def api_dict(self):
        data = {'text': self.text}
        if self.type == 'request_contact':
            data.update({'request_contact': True})
        if self.type == 'request_location':
            data.update({'request_location': True})
        return data


class AnswerCallbackQuery(object):
    """
    Ответ на CallbackQuery.

    query_id: ID callback query
    text: чем отвечать
    """
    def __init__(self, query_id, text):
        self.query_id = query_id
        self.text = str(text)

    def __repr__(self):
        return self.text


class TGSleepMessage(Message):
    """
    Сообщение-пауза. Содержит значение паузы в секундах. Максимум - 20 секунд.
    """

    def __init__(self, seconds, show_typing=False):
        """
        :param seconds: сколько секунд держать паузу
        :param show_typing: показывать ли символ "печатаю..."
        """
        if not isinstance(seconds, int):
            raise StateObjectError(
                'WaitMessage arg must be integer in seconds')
        if seconds > 20:
            seconds = 20
        self.seconds = seconds
        self.show_typing = show_typing

    def __repr__(self):
        string = 'Typing' if self.show_typing else 'Sleep'
        string += ' {}'.format(self.seconds)
        return string

    @staticmethod
    def render(context):
        """ Нечего рендерить. """
        pass

    def api_dict(self):
        return {'wait_seconds': self.seconds}
