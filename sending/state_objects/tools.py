import re
import random


def break_buttons_into_rows(buttons, maxlen=15):
    """
    Переформатировать список кнопок: сделать из него список списков.
    :param buttons: кнопки, которые нужно разбить по спискам
    :param maxlen: макс. длина одного ряда кнопок.
    """
    rows = []
    row = []
    line_length = 0
    for button in buttons:
        line_length += len(button.text)
        # макс кол.во кнопок в ряду Telegram - 8 штук
        if line_length <= maxlen and len(row) <= 8:
            row.append(button)
        else:
            if row:
                rows.append(row)
            row = [button]
            line_length = len(button.text)

    if row:
        rows.append(row)
    return rows


random_pattern = re.compile(r'(\([^|()]+\|[^|)]+[^)]*\))')


def choose_random(string):
    """ Заменяет в сообщения случайные строки. """
    m = re.search(random_pattern, string)
    if m:
        _string = string[:m.start()] + \
                  _get_random_substring(m.group(0)) + \
                  string[m.end():]
        return choose_random(_string)
    else:
        return string


def _get_random_substring(substring):
    """ Выбирает из `(строка1|строка2|строка3)` случайную строку. """
    substring = substring.rstrip(')').lstrip('(')
    variables = substring.split('|')
    return random.choice(variables)
