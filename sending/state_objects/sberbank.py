from django.template import Template

from .tools import choose_random
from ..exceptions import StateObjectError
from .common import Message


class SBTextMessage(Message):
    """
    Текстовое сообщение. Содержит всего один атрибут: текст content.
    """
    def __init__(self, text):
        if not isinstance(text, str):
            raise StateObjectError(
                'Content for TextMessage should be of type str')
        if len(text) > 1000:
            raise StateObjectError(
                'Content for TextMessage must be < 320 symbols')
        self.text = str(text)

    def __repr__(self):
        return self.text[:30]

    def api_dict(self):
        data = {'text': self.text}
        return data

    def not_empty(self):
        if self.text:
            return True

    def render(self, context):
        """Отрендерить контент, вставив туда переменные из контекста.

        :param context: объект класса django.template.Context
        """
        template = Template(choose_random(self.text))
        self.text = str(template.render(context))


class SBImageMessage(Message):
    """
    Сообщение с изображением. Содержит атрибут: url
    """
    def __init__(self, url, caption=None):
        # caption нужен здесь как болванка, потому что иногда этот класс
        # вызывается с caption конструктором сообщений (для унификации
        # с Telegram)
        self.url = url

    def __repr__(self):
        return 'Image: {}...'.format(self.url[:10])

    def api_dict(self):
        data = {'url': self.url}
        return data

    def render(self, context):
        """Рендерить тут нечего"""


class SBSleepMessage(Message):
    """
    Сообщение-пауза. Содержит значение паузы в секундах. Максимум - 20 секунд.
    """

    def __init__(self, seconds, show_typing=False):
        """
        :param seconds: сколько секунд держать паузу
        :param show_typing: показывать ли символ "печатаю..."
        """
        if not isinstance(seconds, int):
            raise StateObjectError('WaitMessage arg must be integer in seconds')
        if seconds > 20:
            seconds = 20
        self.seconds = seconds
        self.show_typing = show_typing

    def __repr__(self):
        string = 'Typing' if self.show_typing else 'Sleep'
        string += ' {}'.format(self.seconds)
        return string

    def render(self, context):
        """Нечего рендерить"""
        pass

    def api_dict(self):
        return {'wait_seconds': self.seconds}


class SBButtonGroup(object):
    """
    Группа кнопок ФБ. Может содержать до 3 кнопок.
    """
    def __init__(self, buttons):
        if not buttons or len(buttons) == 0:
            raise StateObjectError('Cannot have 0 buttons')
        if len(buttons) > 8:
            raise StateObjectError(
                'Number of buttons in SBButtonGroup cannot be > 3')
        self.buttons = buttons

    def __repr__(self):
        return ', '.join(str(button) for button in self.buttons)

    def api_dict(self):
        return [button.api_dict() for button in self.buttons]


class SBButton(object):
    """
    Кнопка ФБ.
    """
    def __init__(self, title, payload):
        self.title = str(title)
        self.payload = payload

    def __repr__(self):
        return self.title

    def api_dict(self):
        data = {'title': self.title, 'command': self.payload}
        return data


class SBCard(object):
    """
    Карточка Сбербанка. Содержит заголовок, тело, ссылку на картинку,
    ссылку на страницу и группу кнопок SBButtonGroup.
    """
    def __init__(self, f1=None, f2=None, f3=None, f4=None, button_group=None,
                 image_url=None):
        for item in (f1, f2, f3, f4):
            if item and len(item) > 1000:
                raise StateObjectError('f1...f4 must be <= 1000')
        if button_group and not isinstance(button_group, SBButtonGroup):
            raise StateObjectError('Wrong type of button group')
        self.f1 = f1
        self.f2 = f2
        self.f3 = f3
        self.f4 = f4
        self.button_group = button_group
        self.image_url = image_url or None  # чтобы не передавались пустые строки

    def __repr__(self):
        return self.f1[:30]

    def api_dict(self):
        if self.button_group:
            buttons = self.button_group.api_dict()
        else:
            buttons = None
        data = {
            'f1': self.f1,
            'f2': self.f2,
            'f3': self.f3,
            'f4': self.f4,
            'images': [self.image_url] if self.image_url else [],
            'menu_items': buttons,
            'title': 'title1'
        }
        # data = {
        #     key: value for key, value in data.items() if value
        # }
        return data


class SBCarouselMessage(Message):
    """
    Сообщение-карусель. Содержит список карточек типа FBCard.
    """
    def __init__(self, cards):
        if len(cards) > 10:
            raise StateObjectError('Number of cards cannot be > 10')
        self.cards = cards

    def __repr__(self):
        return '{} cards'.format(len(self.cards))

    def api_dict(self):
        data = {
            'view_messages': [
                card.api_dict() for card in self.cards
            ]
        }
        return data

    def render(self, context):
        """Отрендерить контент, вставив туда переменные из контекста.

        :param context: объект класса django.template.Context
        """
        for card in self.cards:
            card.f1 = str(Template(card.f1).render(context))
            card.f2 = str(Template(card.f2).render(context))
            card.f3 = str(Template(card.f3).render(context))
            card.f4 = str(Template(card.f4).render(context))

    def not_empty(self):
        if self.cards:
            return True
