from sending.exceptions import StateObjectError
from .viber import VBTextMessage, VBImageMessage, VBDocumentMessage, \
    VBCarouselMessage

from viberbot.api.messages import (
    TextMessage as NativeViberTextMessage,
    PictureMessage as NativeViberPictureMessage,
    FileMessage as NativeViberFileMessage
)


class BotState(object):
    """
    Объект состояния чата.
    Содержит список сообщений, которые нужно отправить юзеру.
    и несколько общих атрибутов:

    - qr_buttons: кнопки быстрого ответа, которые рендерятся вместо клавиатуры
    - answer_callback_query: объект AnswerCallbackQuery. Если он есть,
                то сендер выполнит ответ на callbackquery
    - kill_previous_markups: убивать ли инлайн-кнопки
        всех предыдущих сообщений (для Telegram)
    - extra_context: дополнительные контекстные переменные для рендера.
        Обычно это локальные переменные типа результатов ответов на вопросы,
        которые динамически формируются какой-то из вьюх.
    """
    def __init__(self, messages=[], qr_buttons=None,
                 answer_callback_query=None, kill_previous_markups=None,
                 extra_context=None):
        self.kill_previous_markups = kill_previous_markups
        self.messages = messages
        self.qr_buttons = qr_buttons
        self.answer_callback_query = answer_callback_query
        self.extra_context = extra_context

    def __repr__(self):
        msg_num = len(self.messages)
        string = '[{} msgs]'.format(msg_num)
        return string

    def api_dict(self):
        """
        Сформировать контент в json
        """
        if self.qr_buttons:
            qr_buttons = self.qr_buttons.api_dict()
        else:
            qr_buttons = None
        json_data = {
            'messages': [msg.api_dict() for msg in self.messages],
            'qr_buttons': qr_buttons
        }
        return json_data

    def get_viber_welcome_state_with_1st_msg(self):
        """
        Сгенерировать приветственный стейт в формате, который понимает вайбер.
        Взять только первое сообщение из всех, которые будут в стейте.
        """
        if self.messages:
            message = self.messages[0]
            if isinstance(message, VBTextMessage):
                native_message_dict = NativeViberTextMessage(
                    text=message.text
                ).to_dict()
            elif isinstance(message, VBImageMessage):
                native_message_dict = NativeViberPictureMessage(
                    media=message.url, text=message.caption
                ).to_dict()
            elif isinstance(message, VBDocumentMessage):
                native_message_dict = NativeViberFileMessage(
                    media=message.url, size=10000,
                    file_name=message.get_file_name()
                ).to_dict()
            elif isinstance(message, VBCarouselMessage):
                # Карусель не подходит для приветственного стейта
                # (не читается клиентом вайбера), поэтому просто
                # отдаем пустой словарь
                native_message_dict = {}
            else:
                raise StateObjectError(
                    'Tried to call get_viber_state_with_1st_msg, '
                    'got unknown message type'
                )
        else:
            native_message_dict = {}

        if self.qr_buttons:
            keyboard = self.qr_buttons.api_dict()
        else:
            keyboard = None

        # Здесь у нас должно получиться либо сообщение, либо клавиатура,
        # либо и то и другое. Если нет ни сообщения, ни клавиатуры -
        # такое сообщение не будет отправлено вайбером.
        # Поэтому вызываем здесь исключение.
        if not native_message_dict and not keyboard:
            raise StateObjectError(
                'Trying to create Viber native state without message '
                'AND without keyboard. Please supply at least one of them: '
                'either message or keyboard.'
            )

        native_message_dict.update({'keyboard': keyboard})
        return native_message_dict

    def prepend_messages(self, messages):
        """Присоединить к стейту сообщения в самое начало списка сообщений"""
        if not isinstance(messages, list):
            raise StateObjectError('messages must be of list type')
        self.messages = messages + self.messages

    def append_messages(self, messages):
        """Присоединить к стейту сообщения в самый конец списка сообщений"""
        if not isinstance(messages, list):
            raise StateObjectError('messages must be of list type')
        self.messages = self.messages + messages

    def update_qr_buttons(self, qr_buttons):
        """
        Обновить QR-кнопки стейта на новые кнопки.
        Кнопки обновляются, только если новые кнопки не равны None.
        Иначе, если у добавляемого стейта нет QR-кнопок, а у старого стейта
        они есть, то они просто пропадут.
        """
        if not qr_buttons:
            return
        self.qr_buttons = qr_buttons
