
class SenderBotIsBlockedError(Exception):
    """
    Ошибка, которая выдается при отправке сообщения,
    если пользователь заблокировал бота
    """

class SenderBadContentError(Exception):
    """
    Ошибка, которая выдается при отправке сообщения, если части контента,
    которые предполагается передать, несовместимы друг с другом.
    """

class SenderUserNotFoundError(Exception):
    """
    Ошибка, которая выдается, если пользователь, для которого предназаначалось
    сообщение, не найден в мессенджере по ID.
    """

class StateObjectError(Exception):
    """
    Ошибка, которая вызывается при некорректном формировании объектов
    для стейта: Message, TextMessage, FBCard и т.д.
    """

class BuilderError(Exception):
    """Ошибка в работе билдера сообщений"""